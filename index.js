'use strict'

 const port = process.env.PORT || 3000

//p4
 const https = require('https');
 const fs = require('fs');

const OPTIONS_HTTPS = {
   key: fs.readFileSync('./cert/key.pem'),
   cert: fs.readFileSync('./cert/cert.pem')
};



 const express = require('express');
 const logger = require('morgan');
 const mongojs = require('mongojs');
 // Imports
 const cors = require('cors');
 const app = express();

 var db = mongojs("SD"); // Enlazamos con la DB "SD"
var id = mongojs.ObjectID; // Función para convertir un id textual en un objectID

//errores
 // middlewares
 /*
 var allowMethods = (req, res, next) => {
 res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
 return next();
 };

 var allowCrossTokenHeader = (req, res, next) => {
 res.header("Access-Control-Allow-Headers", "token");
 return next();
 };
*/
//guia 4 pagina 2



//corregido???
// Declaraciones
var allowCrossTokenHeader = (req, res, next) => {
   res.header("Access-Control-Allow-Headers", "*");
   return next();
   };
var allowCrossTokenOrigin = (req, res, next) => {
      res.header("Access-Control-Allow-Origin", "*");
      return next();
      };

 var auth = (req, res, next) => {
if(req.headers.token === "password1234") {
 return next();
 } else {
 return next(new Error("No autorizado"));
 };
 };


//middlewares

app.use(cors());
app.use(allowCrossTokenHeader);
app.use(allowCrossTokenOrigin);

 app.use(logger('dev')); // probar con: tiny, short, dev, common, combined
 app.use(express.urlencoded({extended: false})) // parse application/x-www-form-urlencoded
 app.use(express.json()) // parse application/json


 // routes
 app.param("coleccion", (req, res, next, coleccion) => {
 req.collection = db.collection(coleccion);
 return next();
 });

 app.get('/api', (req, res, next) => {
 db.getCollectionNames((err, colecciones) => {
 if (err) return next(err);
 res.json(colecciones);
 });
 });

 app.get('/api/:coleccion', (req, res, next) => {
 req.collection.find((err, coleccion) => {
if (err) return next(err);
 res.json(coleccion);
 });
 });

 app.get('/api/:coleccion/:id', (req, res, next) => {
 req.collection.findOne({_id: id(req.params.id)}, (err, elemento) => {
 if (err) return next(err);
 res.json(elemento);
 });
 });

 app.post('/api/:coleccion', auth, (req, res, next) => {
 const elemento = req.body;

 if (!elemento.nombre) {
 res.status(400).json ({
 error: 'Bad data',
 description: 'Se precisa al menos un campo <nombre>'
 });
} else {
   req.collection.save(elemento, (err, coleccionGuardada) => {
   if(err) return next(err);
   res.json(coleccionGuardada);
   });
   }
   });
  
   app.put('/api/:coleccion/:id', auth, (req, res, next) => {
   let elementoId = req.params.id;
   req.collection.update({_id: id(elementoId)}, {$set: req.body},
   {safe: true, multi: false}, (err, result) => {
   if (err) return next(err);
   res.json(result);
   });
   });
  
   app.delete('/api/:coleccion/:id', auth, (req, res, next) => {
   let elementoId = req.params.id;
  
   req.collection.remove({_id: id(elementoId)}, (err, resultado) => {
   if (err) return next(err);
   res.json(resultado);
   });
   });
  
  https.createServer(OPTIONS_HTTPS,app).listen(port, () => {
   console.log(`SEC WS API REST ejecutándose en https://localhost:${port}/api/:coleccion/:id`);
   });
//finalizado
//ahora voy a empezar a hacer la p8